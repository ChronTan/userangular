import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';

export class User {
  constructor(
    public name: string,
    public sname: string,
    public age: string,
  ) {}
}

@Injectable({
  providedIn: 'root'
})
export class HttpClientService {


  constructor(
    private httpClient: HttpClient
  ) {
  }

  getUser() {
    console.log('test call');
    return this.httpClient.get<User[]>('http://localhost:8080/users');
  }

  public deleteUser(user) {
    return this.httpClient.delete<User>("http://localhost:8080/users" + "/" + user.name);
  }

  public createUser(user) {
    return this.httpClient.post<User>('http://localhost:8080/users', user);
  }

}
